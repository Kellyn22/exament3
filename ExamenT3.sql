USE [ExamenT3]
GO
/****** Object:  Table [dbo].[Etiqueta]    Script Date: 06/11/2021 23:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Etiqueta](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
 CONSTRAINT [PK_Etiqueta] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EtiquetaNota]    Script Date: 06/11/2021 23:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EtiquetaNota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEtiqueta] [int] NULL,
	[IdNota] [int] NULL,
 CONSTRAINT [PK_EtiquetaNota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Nota]    Script Date: 06/11/2021 23:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Titulo] [nvarchar](100) NULL,
	[Fecha] [datetime] NULL,
	[Contenido] [nvarchar](1000) NULL,
	[IdUser] [int] NULL,
 CONSTRAINT [PK_Nota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 06/11/2021 23:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Etiqueta] ON 

INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (1, N'Informática')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (2, N'Física')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (3, N'Historia')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (4, N'Matemática')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (5, N'Química')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (6, N'Cultura')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (7, N'Enseñanza')
INSERT [dbo].[Etiqueta] ([Id], [Nombre]) VALUES (8, N'Virtual')
SET IDENTITY_INSERT [dbo].[Etiqueta] OFF
SET IDENTITY_INSERT [dbo].[EtiquetaNota] ON 

INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (1, 1, 1)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (2, 3, 2)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (3, 6, 2)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (4, 7, 2)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (5, 8, 2)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (6, 6, 3)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (7, 7, 3)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (14, 7, 5)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (15, 8, 5)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (16, 6, 6)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (17, 7, 6)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (18, 1, 7)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (19, 2, 7)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (20, 4, 7)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (21, 7, 7)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (22, 8, 7)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (23, 7, 8)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (24, 7, 9)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (25, 8, 9)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (26, 6, 10)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (27, 7, 10)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (28, 8, 10)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (29, 7, 11)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (30, 8, 11)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (31, 2, 12)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (32, 3, 12)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (33, 4, 12)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (34, 1, 13)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (35, 4, 13)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (36, 8, 13)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (39, 7, 15)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (40, 4, 16)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (41, 1, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (42, 2, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (43, 3, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (44, 4, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (45, 5, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (46, 6, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (47, 7, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (48, 8, 17)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (51, 3, 19)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (52, 1, 20)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (53, 2, 20)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (54, 7, 20)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (55, 8, 20)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (60, 7, 23)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (61, 8, 23)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (62, 5, 24)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (63, 7, 24)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (64, 1, 25)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (65, 7, 25)
INSERT [dbo].[EtiquetaNota] ([Id], [IdEtiqueta], [IdNota]) VALUES (66, 8, 25)
SET IDENTITY_INSERT [dbo].[EtiquetaNota] OFF
SET IDENTITY_INSERT [dbo].[Nota] ON 

INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (1, N'Sistemas', CAST(N'2021-07-06 22:05:37.287' AS DateTime), N'Desarrolla tu formación y conocimientos en el área de las ciencias de la computación, ingeniería de software y nuevas tecnologías de información.
', 1)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (2, N'Derecho', CAST(N'2021-07-06 22:06:17.183' AS DateTime), N'Especialízate en negocios internacionales, y serás capaz de gestionar y administrar todo tipo de operaciones internacionales de forma eficiente y competitiva.
', 2)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (3, N'Socialización ', CAST(N'2021-07-07 00:51:48.347' AS DateTime), N'La socialización es', 3)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (5, N'Becas UPN 2045', CAST(N'2021-07-07 01:02:46.517' AS DateTime), N'¿Tienes dudas sobre las becas?', 1)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (6, N'Agenda Cultural 2', CAST(N'2021-07-06 22:43:58.687' AS DateTime), N'La programación de las actividades de Jueves Culturales se publicará en nuestras redes sociales.', 2)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (7, N'Exámenes Finales ', CAST(N'2021-07-06 22:17:24.913' AS DateTime), N'Se estima que sean entren las semanas 15 y 16 del presente ciclo académico.', 3)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (8, N'Finanzas en ESAN', CAST(N'2021-07-06 22:19:23.033' AS DateTime), N'El descuento de flujos de caja para la valorización de empresas y proyectos.', 1)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (9, N'UNC', CAST(N'2021-07-06 23:58:34.207' AS DateTime), N'Portal transparencia UNC. capacitación a nuevos docentes 2021', 2)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (10, N'Biología 2', CAST(N'2021-07-07 00:52:26.100' AS DateTime), N'Ciencia que estudia la estructura de los seres vivos.', 3)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (11, N'Ing de Sistemas', CAST(N'2021-07-07 02:58:57.840' AS DateTime), N'Formación de exigencia', 0)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (12, N'Economía de la UPN', CAST(N'2021-07-07 20:45:55.667' AS DateTime), N'Carrera presente en la UPN CAJAMARCA.', 6)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (13, N'Ing Cívil y de Costos ', CAST(N'2021-07-07 03:10:01.457' AS DateTime), N'Carrera con Costos y Presupuestos I & II', 6)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (15, N'UNC Sede Cajamarca', CAST(N'2021-07-07 16:13:58.790' AS DateTime), N'Universidad Nacional de Cajamarca cumple 149 ', 5)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (16, N'Ing Industrial', CAST(N'2021-07-07 16:14:19.577' AS DateTime), N'Carrera de ámbito profesional y académico de todas las épocas.', 5)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (17, N'UPN SEDE CAJAMARCA ', CAST(N'2021-07-07 16:35:44.670' AS DateTime), N'Universidad Privada del Norte desde los años 90 y actualmente se mantiene activa.', 6)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (19, N'Historia del Perú', CAST(N'2021-07-07 14:49:41.860' AS DateTime), N'Se trata toda la historia a lo largo de los años.', 7)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (20, N'UNMSM', CAST(N'2021-07-07 15:28:53.750' AS DateTime), N'Ubicación en la ciudad de Lima.', 5)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (23, N'UPAO', CAST(N'2021-07-07 20:48:08.057' AS DateTime), N'Universidad de Prestigio en Trujillo 2021.', 6)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (24, N'Ecología Virtual', CAST(N'2021-07-07 20:53:14.177' AS DateTime), N'Precisamente el problema fundamental, no solo de Breath of the Wild, sino del diseño de casi todos los ecosistemas.', 7)
INSERT [dbo].[Nota] ([Id], [Titulo], [Fecha], [Contenido], [IdUser]) VALUES (25, N'WordPress', CAST(N'2021-07-07 20:54:13.613' AS DateTime), N'Administrar tu panel de control.', 7)
SET IDENTITY_INSERT [dbo].[Nota] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Username], [Password]) VALUES (5, N'pepe', N'YmB6+DZtJU9NZIuYc16amtAu6QsOWxOB38viSf3FQ5A=')
INSERT [dbo].[User] ([Id], [Username], [Password]) VALUES (6, N'pepe2', N'YmB6+DZtJU9NZIuYc16amtAu6QsOWxOB38viSf3FQ5A=')
INSERT [dbo].[User] ([Id], [Username], [Password]) VALUES (7, N'pepe3', N'YmB6+DZtJU9NZIuYc16amtAu6QsOWxOB38viSf3FQ5A=')
SET IDENTITY_INSERT [dbo].[User] OFF
